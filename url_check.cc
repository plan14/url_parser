#include "url.h"

#if URL_PARSER_URL_CHECK
#include <cstring>
using namespace std;

// Update valid flag
void url::check() {}

// Component validation
bool url::check_userinfo() {}

// RFC 3986 character classes
inline bool url::is_unreserved(int ch) {
  return isalnum(ch) || !!strchr("-._~", ch);
}

inline bool url::is_reserved(int ch) {
  return is_gen_delim(ch) || is_sub_delim(ch);
}

inline bool url::is_gen_delim(int ch) { return !!strchr(":/?#[]@", ch); }

inline bool url::is_sub_delim(int ch) { return !!strchr("!$&'()*+,;=", ch); }

// Validate percent-encoded character
bool url::is_pct_encoded(const char *p) {
  return p[0] == '%' && isxdigit(p[1]) && isxdigit(p[2]);
}

#endif
