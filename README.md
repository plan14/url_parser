# URL parser
Specification here: https://datatracker.ietf.org/doc/html/rfc3986

This is a simple URL parser that splits a URL into its component parts:
- scheme
- username
- password
- host
- port
- path
- query
- fragment

The API is simple:
1. `url(const std:string&)` constructor makes a URL object out of a string, and
the component parts become available in respective member variables.
2. `to_string()` member function composes the url object back into a string.
3. `bool operator==(const url &other)` compares the component parts of two URLs.

Features and limitations:
1. *scheme* and *host* components are converted to lower case, per RFC.
2. IPv6 hosts are recognized, but not yet-undefined IP formats with format
   version encoded.
3. No URL encoding.
4. No checking for valid character sets for any components except *scheme*,
   where it's unavoidable.

NB: limitations #2, 3, and 4 above may be fixed in the future.
