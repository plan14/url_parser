// URL parser
#pragma once
#include <string>

#ifndef URL_PARSER_URL_CHECK
#define URL_PARSER_URL_CHECK 1
#endif

class url {
public:
  std::string scheme;
  std::string username;
  std::string password;
  std::string host;
  std::string port;
  std::string path;
  std::string query;
  std::string fragment;
  bool valid{};

  url(const std::string &u);
  std::string to_string() const;
  bool operator==(const url &other);

private:
  // Convert ASCII string to lower case
  static void lower_case(std::string &s);

  // Like strchr, but returns past-end pointer when fails
  static const char *strpchr(const char *p, int c);

#if URL_PARSER_URL_CHECK
  // Update valid flag
  void check();

  // Component validation
  bool check_userinfo();

  // RFC 3986 character classes
  static bool is_unreserved(int ch);
  static bool is_reserved(int ch);
  static bool is_gen_delim(int ch);
  static bool is_sub_delim(int ch);

  // Validate percent-encoded character
  bool is_pct_encoded(const char *p);
#endif
};
