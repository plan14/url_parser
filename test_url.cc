#include "url.h"
#include <cstring>
#include <fmt/core.h>
#include <string>
using namespace std;

static const char *tests[] = {
    "htTp://User:Pass@WWW.url.com:555/dir/Dir2?Query#Fragment",
    "http://www.url.com/dir/dir2?query#fragment",
    "http://user@www.url.com/dir/dir2?query#fragment",
    "file:url.cc",
    "file:/url.cc",
    "//www.url.com/dir/dir2?query#fragment",
    "url.cc",
    "/url.cc",
    "#fr",
    "?qu",
    "//[123:345]:555",
    "//a[123:345]:555",
    "//[123:345]a:555"};

void print_url(const url &u) {
  fmt::print(
      "{}\n"
      "  sc:[{}] us:[{}] pa:[{}] ho:[{}] po:[{}] pa:[{}] qu:[{}] fr:[{}]\n",
      u.to_string(), u.scheme, u.username, u.password, u.host, u.port, u.path,
      u.query, u.fragment);
}

static int stricmp(const char *a, const char *b) {
  for (;; a++, b++) {
    const int d = tolower(*a) - tolower(*b);
    if (d != 0 || !*a)
      return d;
  }
}

int main(int argc, char *argv[]) {
  auto x = tests[3];
  bool ok{true};
  for (auto &t : tests) {
    url u{t};
    print_url(u);
    ok &= !stricmp(u.to_string().c_str(), t);
    ok &= (u == url(x)) == !strcmp(t, x);
    ok &= (u != url(x)) == !!strcmp(t, x);
  }
  fmt::print("{}\n", ok ? "pass" : "fail");
  return ok ? 0 : -1;
}
