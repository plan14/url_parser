// URL parser
#include "url.h"
#include <algorithm>
#include <cctype>
#include <cstring>
using namespace std;

url::url(const string &u) {
  string auth, userinfo;
  const char *p, *q, *r;

  /* Parse URL string */

  // Scheme
  p = u.c_str();
  q = p;
  if (isalpha(*q++)) {
    while (isalnum(*q) || strchr("+-.", *q))
      ++q;
    if (*q == ':') {
      scheme = string(p, q);
      lower_case(scheme);
      p = q + 1;
    }
  }

  // Authority
  if (!strncmp(p, "//", 2)) {
    p += 2;
    q = p + strcspn(p, "/?#");
    auth = string(p, q);
    p = q;
  }

  // Path
  q = p + strcspn(p, "?#");
  path = string(p, q);
  p = q;

  // Query
  if (*p == '?') {
    ++p;
    q = strpchr(p, '#');
    query = string(p, q);
    p = q;
  }

  // Fragment
  if (*p == '#')
    fragment = string(p + 1);

  /* Parse authority */

  // User info
  p = auth.c_str();
  q = strchr(p, '@');
  if (q) {
    userinfo = string(p, q);
    p = q + 1;
  }

  // IPv6+ host check
  q = p;
  if (*p == '[') {
    r = strchr(p, ']');
    if (r) {
      ++r;
      if (!*r || *r == ':')
        q = r;
    }
  }

  // Host
  q = strpchr(q, ':');
  host = string(p, q);
  lower_case(host);
  p = q;

  // Port
  if (*p == ':')
    port = string(p + 1);

  /* Parse user info */

  // User name
  p = userinfo.c_str();
  q = strpchr(p, ':');
  username = string(p, q);

  // Password
  if (*q == ':')
    password = string(q + 1);
}

string url::to_string() const {
  string s;

  // Scheme
  if (!scheme.empty())
    s += scheme + ':';

  // User info
  string auth = username;
  if (!password.empty())
    auth += ':' + password;
  if (!auth.empty())
    auth += '@';

  // Host+port
  auth += host;
  if (!port.empty())
    auth += ':' + port;

  // Authority
  if (!auth.empty())
    s += "//" + auth;

  // Path
  if (!path.empty())
    s += path;

  // Query
  if (!query.empty())
    s += '?' + query;

  // Fragment
  if (!fragment.empty())
    s += '#' + fragment;
  return s;
}

bool url::operator==(const url &other) {
  return scheme == other.scheme && username == other.username &&
         password == other.password && host == other.host &&
         port == other.port && path == other.path && query == other.query &&
         fragment == other.fragment;
}

/*
 * Private functions
 */

// Convert ASCII string to lower case
void url::lower_case(string &s) {
  transform(s.begin(), s.end(), s.begin(), [](int c) { return tolower(c); });
}

// Like strchr, but returns past-end pointer when fails
const char *url::strpchr(const char *p, int c) {
  const char *q = strchr(p, c);
  if (!q)
    q = p + strlen(p);
  return q;
}
